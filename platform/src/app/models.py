from app import db, login_mng
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin


class BaseMixin(object):

    def submit(self):
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()

    @classmethod
    def create(cls, **kwargs):
        obj = cls(**kwargs)
        try:
            db.session.add(obj)
            db.session.commit()
        except:
            db.session.rollback()

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
        except:
            db.session.rollback()


class User(BaseMixin, db.Model, UserMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    bot_id = db.Column(db.Integer, unique=True)
    email = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(128))
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    profile_pic = db.Column(db.String(256))
    access_level = db.Column(db.Integer, default=0)

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def get_id(self):
        return self.id

    def get_user(user_id):
        user = db.session.query(User).filter(User.bot_id == user_id).first()
        if not user:
            user = User(bot_id=user_id)
        return user

    def __repr__(self):
        return '<User id={}, bot_id={}, first_name={}, access_level={}, email={}>' \
            .format(self.id, self.bot_id, self.first_name, self.access_level, self.email)


def create_test_user():
    u = User(email='asd')
    u.set_password('asd')
    db.session.add(u)
    db.session.commit()


@login_mng.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
