import sys
import requests
from os import environ
from app import app, db
from app.models import User
from subprocess import check_output
from helpers import admin_required
from app.forms import LoginForm, RegisterForm
from flask import render_template, request, redirect, url_for, flash
from flask_login import current_user, login_user, logout_user, login_required


@app.route('/')
@login_required
def index():
    return render_template("index.html", user=current_user)


@app.route('/admin')
@login_required
@admin_required
def admin():
    return render_template("admin.html", user=current_user)


@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegisterForm(csrf_enabled=False)

    if request.method == 'POST':

        user_id = request.form.get('user_id')
        if form.validate_on_submit():
            print(request.form, file=sys.stderr)
            # register user with the id
            user_data = get_user_data(user_id)
            print('DAAADDADAAD')

            u = db.session.query(User).filter(User.bot_id == user_data['user_id']).first()

            if not u:
                u = User(email=user_data['user_id'], profile_pic=user_data['profile_pic'],
                         first_name=user_data['first_name'], last_name=user_data['last_name'])

            u.email = form.email.data
            u.set_password(form.password.data)

            u.submit()
            login_user(u, remember=True)
            return redirect(url_for('login'))
        else:
            print(form.errors, file=sys.stderr)
            for field_name, error_message in form.errors.items():
                flash(error_message[0])
                break

        return render_template("default/register.html", title='Register', form=form, user_id=user_id)

    return render_template("default/register.html", title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if request.method == 'POST':
        user = User.query.filter_by(email=form.email.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password', 'danger')
            return redirect('/')
        login_user(user, remember=form.remember_me.data)
        return redirect('/')
    return render_template('default/login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect('/')


def get_container_ip(cont_name):
    out = check_output(["dig", "+short", cont_name])
    out = out.decode("utf-8")
    out = out.split('\n', 1)[0]
    return out if out is not "" else None


def get_user_data(fb_id):
    data = {
        "fb_id": fb_id
    }

    # get container ip BECAUSE Apache!
    name = environ.get("CHATBOT_NAME", default=None)
    cont_ip = get_container_ip(name)

    out = requests.get("http://{}/user".format(cont_ip), params=data)

    if not out:
        return None

    out = out.json()
    print(out, file=sys.stderr)
    return out
