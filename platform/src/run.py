from app import app, db, models
import sys


@app.shell_context_processor
def make_shell_context():
    return {
        'db': db,
        'md': models,
    }


if __name__ == '__main__':
    sys.path.append("/platform/src")
    app.run(host='0.0.0.0', port=80, debug=True)
